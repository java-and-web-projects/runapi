var mainUrl =
  "https://runescape.wiki/api.php?action=query&format=json&list=categorymembers&cmtitle=Category:Bestiary&cmlimit=100&origin=*";
var currentEl = null;
var currentInnertext = null;
function mainPageLoad() {
  getDataFromApi(mainUrl, extractid);
}
function getDataFromApi(pageUrl, func) {
  var runscapApiFetch = new XMLHttpRequest();
  runscapApiFetch.open("GET", pageUrl, true);
  runscapApiFetch.setRequestHeader("sec-fetch-dest", "document");
  runscapApiFetch.setRequestHeader(
    "accept",
    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8"
  );
  runscapApiFetch.onreadystatechange = function () {
    if (this.status == 200) {
      if (this.readyState == 4) {
        var runscapeData = JSON.parse(runscapApiFetch.responseText);
        func(runscapeData);
        document.getElementById("loading").innerHTML = "";
      } else {
        document.getElementById("loading").innerHTML = "loading";
      }
    } else {
      document.getElementById("loading").innerHTML =
        "can not connect to website";
    }
  };
  runscapApiFetch.send();
}
function extractid(parsedData) {
  if (
    document.getElementsByClassName("loadmore")[0] != undefined &&
    document.getElementsByClassName("loadmore")[0] != null
  ) {
    document
      .getElementById("contentBox")
      .removeChild(document.getElementsByClassName("loadmore")[0]);
  }

  var imgUrl =
    "https://runescape.wiki/api.php?action=query&format=json&origin=*&prop=pageimages&piprop=original&pageids=";
  for (page of parsedData.query.categorymembers) {
    getDataFromApi(imgUrl + page.pageid, creatElement);
  }
  setTimeout(function () {
    var boxElement = document.createElement("a");
    boxElement.className = "q loadmore";
    boxElement.onclick = function () {
      getDataFromApi(
        mainUrl + "&cmcontinue=" + parsedData.continue.cmcontinue,
        extractid
      );
    };
    boxElement.innerHTML =
      "<article><span>  LOAD    ~50    MORE  </span></article>";
    document.getElementById("contentBox").appendChild(boxElement);
    $(".q").off("click");
    $(".q").click(function (e) {
      $(this).toggleClass("q");
      $(this).toggleClass("activated");
      if (this.getElementsByClassName("wikiText")[0] == undefined) {
        getDataFromApi(
          "https://runescape.wiki/api.php?action=parse&format=json&origin=*&pageid=" +
            this.name,
          createWiki
        );
        currentEl = this;
      } else {
        this.childNodes[0].innerHTML = currentInnertext;
        currentInnertext = null;
      }
    });
  }, 2000);
}

function creatElement(parsedData) {
  var boxElement = document.createElement("a");
  boxElement.className = "q";
  boxElement.href = "#";
  for (first in parsedData.query.pages) {
    if (parsedData.query.pages[first].original != undefined) {
      boxElement.name = first;
      boxElement.innerHTML =
        '<article><img class="img-fluid rounded mx-auto d-block" src="' +
        parsedData.query.pages[first].original.source +
        '" class="img-fluid ${3|rounded-top,rounded-right,rounded-bottom,rounded-left,rounded-circle,|}" alt=""><span class="titletext mx-auto d-block">' +
        parsedData.query.pages[first].title +
        "</span></article>";
      document.getElementById("contentBox").appendChild(boxElement);
    }
  }
}
function createWiki(parsedData) {
  var boxElement = document.createElement("div");
  boxElement.className = "wikiText mx-auto d-inline overflow-auto";
  var text = parsedData.parse.text["*"];
  text = text.replace(/href="/g, 'href="https://runescape.wiki');
  text = text.replace(/src="/g, 'src="https://runescape.wiki');
  text = text.replace(/srcset="/g, 'srcset="https://runescape.wiki');
  boxElement.innerHTML = text;
  currentInnertext = currentEl.childNodes[0].innerHTML;
  currentEl.childNodes[0].innerHTML = boxElement.outerHTML;
}
function searcher() {
  closeActive();
  var inputSearch = document.getElementById("searcherid").value;
  if (inputSearch == null || ("" + inputSearch).length < 3) {
    $("#contentBox").removeClass("w-100");
    for (childs of document.getElementsByClassName("q")) {
      childs.style.display = "block";
    }
  } else {
    $("#contentBox").addClass("w-100");
    for (childs of document.getElementsByClassName("q")) {
      if (
        (childs.childNodes[0].childNodes[1].innerHTML + "")
          .toLowerCase()
          .includes(inputSearch.toLowerCase())
      ) {
        childs.style.display = "block";
      } else {
        childs.style.display = "none";
      }
    }
  }
}
function closeActive() {
  if (currentInnertext != null) {
    currentEl.click();
  }
}
